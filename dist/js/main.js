"use strict";

document.addEventListener("DOMContentLoaded", function (event) {
  var headerDropdown = document.querySelector(".account-field");
  headerDropdown.addEventListener("click", function (event) {
    if (event.target.parentElement.parentElement.classList.contains('currency')) {
      event.target.parentElement.classList.toggle('active');
    } else {
      event.target.parentElement.classList.toggle('active');
    }
  });
});